<?php
/**
 * @package Custom Car Plugin
 * @version 1.0
 */
/*
Plugin Name: Custom Car Plugin
Plugin URI: http://radiation3.com/
Description: This plugin is used to manage Car information.
Author: Radiation 3
Version: 1.0
Author URI: http://radiation3.com/
*/

include_once ('plugin_function.php');
include_once('simplexlsx.class.php');

function custom_car_activate(){
	global $wpdb;
	$charset_collect = $wpdb->get_charset_collate();
	$table_brand = $wpdb->prefix . "brand";
	$table_car_info = $wpdb->prefix . "car_info";

	$sql0 = "CREATE TABLE IF NOT EXISTS `$table_brand` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_name` text NOT NULL,
  `b_logo` text NOT NULL,
  PRIMARY KEY (`b_id`)
) $charset_collect;";


	$sql1 = "CREATE TABLE IF NOT EXISTS `$table_car_info` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_id` int(11) NOT NULL ,
  `m_name` text NOT NULL ,
  `c_country` text NOT NULL ,
  `c_url` text NOT NULL ,
  PRIMARY KEY (`c_id`)
) $charset_collect;";


    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql0 );
	dbDelta( $sql1 );


	$array_of_brand = array(
		array("b_name" => "Acura",
		      "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Acura-Logo.jpg",
		),array("b_name" => "Aixam",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Aixam.jpg",
		),array("b_name" => "Alfa Romeo",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Alfa-Romeo.jpg",
		),array("b_name" => "Aston Martin",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Aston-Martin.png",
		),array("b_name" => "Audi",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/audi-logo.jpg",
		),array("b_name" => "Austin",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/d19e330dab951ff3025a685968974550.jpg",
		),array("b_name" => "Bedford ",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Bedford.jpg",
		),array("b_name" => "Bentley",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Bentley-Logo.jpg",
		),array("b_name" => "Bmw",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/bmw-logo1.jpg",
		),array("b_name" => "British Leyland",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/British-Leyland.jpg",
		),array("b_name" => "Bugatti",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Bugatti.png",
		),array("b_name" => "Buick",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Buick-Logo.jpg",
		),array("b_name" => "Byd",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/byd_logo_new_1.jpg",
		),array("b_name" => "Cadillac",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Cadillac-Logo.jpg",
		),array("b_name" => "Chevrolet",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Chevrolet-Logo.jpg",
		),array("b_name" => "Chrysler",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Chrysler-Logo.jpg",
		),array("b_name" => "Citreon",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/citroen-logo.jpg",
		),array("b_name" => "Citroen",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/citroen-logo.jpg",
		),array("b_name" => "Daewoo",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Daewoo.jpg",
		),array("b_name" => "Daihatsu",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/daihatsu-logo-5.jpg",
		),array("b_name" => "Daimler",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Daimler.jpg",
		),array("b_name" => "Datsun",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Datsun_brand_logo.png",
		),array("b_name" => "Dodge",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/dodge_9_105108.jpg",
		),array("b_name" => "Ferrari",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/ferrari-logo.jpg",
		),array("b_name" => "Fiat",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/fiat-logo.jpg",
		),array("b_name" => "Ford",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/ford-logo.jpg",
		),array("b_name" => "Gmc",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/GMC-logo-5.jpg",
		),array("b_name" => "Hillman",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Hillman.gif",
		),array("b_name" => "Holden",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/holden-logo-2.jpg",
		),array("b_name" => "Honda",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/honda-logo.jpg",
		),array("b_name" => "Hummber",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/humber_logo.jpg",
		),array("b_name" => "Hyundai",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/hyundai-logo.jpg",
		),array("b_name" => "Infiniti",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Infiniti-Logo.jpg",
		),array("b_name" => "Irisbus",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Irisbus_logo.png",
		),array("b_name" => "Isuzu",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/isuzu_logo.jpg",
		),array("b_name" => "Iveco",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Iveco.jpg",
		),array("b_name" => "Jaguar",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/jaguar-cars-logo1.jpg",
		),array("b_name" => "Jeep",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Jeep-Logo.jpg",
		),array("b_name" => "Kia",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Kia.jpg",
		),array("b_name" => "Lada",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/LADA_Logo-1.jpg",
		),array("b_name" => "Lamborghini",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Lamborghini.jpg",
		),array("b_name" => "Lancia",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Lancia_Logo.jpg",
		),array("b_name" => "Land Rover",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Land-Rover.jpg",
		),array("b_name" => "Lexus",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/lexus.jpg",
		),array("b_name" => "Leyland",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/leyland_society_logo.gif",
		),array("b_name" => "Lincoln",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Lincoln_Motor_Company_logo.gif",
		),array("b_name" => "London Taxi",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/resources-LTC-Logo.jpg",
		),array("b_name" => "Lotus",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/lotus-logo.png",
		),array("b_name" => "Maserati",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Maserati.jpg",
		),array("b_name" => "Maybach",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/maybach-logo-2.jpg",
		),array("b_name" => "Mazda",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/mazda_logo.png",
		),array("b_name" => "Mclaren",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/mclaren-logo-2.jpg",
		),array("b_name" => "Mercedes",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/mercedes-benz-logo-design.jpg",
		),array("b_name" => "Mercury",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/mercury-automobile.jpg",
		),array("b_name" => "Mg",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/mg_logo.png",
		),array("b_name" => "Microcar",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/MICROCAR_LOGO.jpg",
		),array("b_name" => "Mini",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Mini-logo-4.jpg",
		),array("b_name" => "Mitsubishi",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Mitsubishi-Motors-Logo.jpg",
		),array("b_name" => "Morgan",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/morgan.png",
		),array("b_name" => "Morris",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Morris-Motors-logo-2.jpg",
		),array("b_name" => "Moto Guzzi",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Moto-Guzzi.jpg",
		),array("b_name" => "Moto Roma",
		        "b_logo" => "http://www.manchestermotorbikecentre.co.uk/large/MR124446.jpg",
		),array("b_name" => "Nissan",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/nissan-logo.jpg",
		),array("b_name" => "Oldsmobile",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Oldsmobile_Logo.jpg",
		),array("b_name" => "Opel",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/opel-logo-Z.jpg",
		),array("b_name" => "Peugeot",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Peugeot-Logo.jpg",
		),array("b_name" => "Plymouth",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Plymouth-car-logo-2.jpg",
		),array("b_name" => "Polaris",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/polaris-logo-disclaimer.png",
		),array("b_name" => "Pontiac",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/pontiac.jpg",
		),array("b_name" => "Porsche",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Porsche-Logo.jpg",
		),array("b_name" => "Proton",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/proton-logo.png",
		),array("b_name" => "Range Rover",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Range-Rover.png",
		),array("b_name" => "Renault",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/renadult-logo.jpg",
		),array("b_name" => "Rolls-Royce",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Rolls-Royce-Logo.png",
		),array("b_name" => "Rover",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Rover-logo-1.jpg",
		),array("b_name" => "Rover Mg",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/MG_Rover_Group_logo.jpg",
		),array("b_name" => "Saab",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Saab-Logo.jpg",
		),array("b_name" => "Saturn",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/saturn-cars-logo-emblem.jpg",
		),array("b_name" => "Scion",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/scion-logo.png",
		),array("b_name" => "Seat",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/seat.jpg",
		),array("b_name" => "Skoda",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Skoda.jpg",
		),array("b_name" => "Smart",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/smart.jpg",
		),array("b_name" => "Ssangyong",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/SsangYong_Motor_Company.svg_.png",
		),array("b_name" => "Subaru",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Subaru_logo.svg_.png",
		),array("b_name" => "Sunbeam",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Sunbeam.jpg",
		),array("b_name" => "Suzuki",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/suzuki-cars-logo-emblem.jpg",
		),array("b_name" => "Talbot",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/TALBOT-SIMCA.jpg",
		),array("b_name" => "Tata",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/TATA-Motors.jpg",
		),array("b_name" => "Tesla",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/tesla-cars-logo-emblem.jpg",
		),array("b_name" => "Toyota",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/toyota-logo11.jpg",
		),array("b_name" => "Triumph",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Triumph.jpg",
		),array("b_name" => "TVR",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/tvr2.jpg",
		),array("b_name" => "Vauxhall",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Vauxhall_Logo.jpg",
		),array("b_name" => "Volkswagen",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Wolksvagen-Logo1.jpg",
		),array("b_name" => "Volvo",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/volvo-logo-200x200.jpg",
		),array("b_name" => "Xinling",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Xinling.jpg",
		),array("b_name" => "Yamaha",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Yamaha-Bike-Logo-Download-Free.jpg",
		),array("b_name" => "Yugo",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/yugo.jpg",
		),array("b_name" => "Zastava",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Zastava_Special_Cars...Sombor.jpg",
		),array("b_name" => "Zenos",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/Zenos.png",
		),array("b_name" => "Zongshen",
		        "b_logo" => "http://autofigurator.com/wp-content/uploads/2016/03/ZS-logo-1.png",
		),
	);

	foreach ($array_of_brand as $brand)
	$wpdb->insert($table_brand ,$brand );



}

register_activation_hook(__FILE__ , 'custom_car_activate');


function custom_car_deactivation(){

    global $wpdb;
    $table_brand = $wpdb->prefix . "brand";
    $table_car_info = $wpdb->prefix . "car_info";

    $sql0 = "DROP TABLE `$table_brand`";
    $sql1 = "DROP TABLE `$table_car_info`";


    $wpdb->query($sql0);
    $wpdb->query($sql1);
}

register_deactivation_hook(__FILE__ , 'custom_car_deactivation');


add_action('admin_menu','custom_car_admin_menu');

function custom_car_admin_menu(){
	add_menu_page('Dashboard', 'AutoConfigrator' , 'manage_options' , 'custom_car_main_menu' ,'custom_car_engine');
	add_submenu_page('custom_car_main_menu' ,'Brand', 'Brand' , 'manage_options' , 'custom_car_brand' ,'custom_car_brand' );
	//add_submenu_page('custom_car_main_menu' ,'Model', 'Model' , 'manage_options' , 'custom_car_model' ,'custom_car_model' );
	//add_submenu_page('custom_car_main_menu' ,'Generation', 'Generation' , 'manage_options' , 'custom_car_generation' ,'custom_car_generation' );
	//add_submenu_page('custom_car_main_menu' ,'Engine', 'Engine' , 'manage_options' , 'custom_car_engine' ,'custom_car_engine' );
	//add_submenu_page('#' ,'Review', 'Review' , 'manage_options' , 'custom_car_review' ,'custom_car_review' );

}

function custom_car_dashboard(){
	$page_url = get_admin_url()."admin.php?page=custom_car_brand";
	echo "<script>
			//window.location.assign('$page_url');
	</script>";

    include_once('simplexlsx.class.php');
    include('admin_include/file_import_export.php');
}

function custom_car_brand(){
	include_once ('admin_include/brand.php');
}

function custom_car_engine(){
	include_once ('admin_include/engine.php');
}

//add_action('wp_enqueue_scripts' , 'custom_car_style');
add_action('admin_enqueue_scripts' , 'custom_car_style');

function custom_car_style(){
	wp_enqueue_style('custom_car_style', plugin_dir_url(__FILE__).'assets/custom_car.css');
	wp_enqueue_style('alertify_css', plugin_dir_url(__FILE__).'alertify/css/alertify.min.css');
	//wp_enqueue_style('custom_bootstrap', plugin_dir_url(__FILE__).'bootstrap/css/bootstrap.css');

    wp_enqueue_script( 'alertify_js', plugin_dir_url(__FILE__) . '/alertify/alertify.min.js' , array('jquery'));
    wp_enqueue_script( 'my_custom_script', plugin_dir_url(__FILE__) . '/admin_include/main.js' , array('jquery', 'alertify_js'));
}

add_action( 'wp_ajax_get_result_for_car', 'get_result_for_car' );
add_action( 'wp_ajax_nopriv_get_result_for_car', 'get_result_for_car' );
function get_result_for_car() {
    global $wpdb; // this is how you get access to the database
    $b_id = $_POST['brand_id'];
    $model = $_POST['model_name'];
    $country = $_POST['country_name'];
    $table_car_info = $wpdb->prefix .'car_info';
    $all_cars = $wpdb->get_results("SELECT * FROM $table_car_info WHERE `b_id` = $b_id AND `m_name` = '$model' AND `c_country` = '$country' ");

    print_r(json_encode($all_cars));
}




add_action( 'wp_ajax_delete_brand_ajax', 'delete_brand_ajax' );
add_action( 'wp_ajax_nopriv_delete_brand_ajax', 'delete_brand_ajax' );
function delete_brand_ajax() {
    global $wpdb; // this is how you get access to the database
    $b_id = $_POST['brand_id'];
    $table_brand = $wpdb->prefix .'brand';
    $brand = $wpdb->get_results("DELETE FROM $table_brand WHERE `b_id` = $b_id");
}



add_action( 'wp_ajax_delete_car_ajax', 'delete_car_ajax' );
add_action( 'wp_ajax_nopriv_delete_car_ajax', 'delete_car_ajax' );
function delete_car_ajax() {
    global $wpdb; // this is how you get access to the database
    $c_id = $_POST['car_id'];
    $table_car_info = $wpdb->prefix .'car_info';
    $car = $wpdb->get_results("DELETE FROM $table_car_info WHERE `c_id` = $c_id");
}

add_action( 'wp_ajax_get_country_by_brand_and_model', 'get_country_by_brand_and_model' );
add_action( 'wp_ajax_nopriv_get_country_by_brand_and_model', 'get_country_by_brand_and_model' );
function get_country_by_brand_and_model() {
    global $wpdb; // this is how you get access to the database
    $b_id = intval( $_POST['brand_id'] );
    $model = intval( $_POST['model_name'] );
    $table_car_info  = $wpdb->prefix .'car_info';
    $country_name = $wpdb->get_results("SELECT DISTINCT `c_country` FROM $table_car_info WHERE `b_id` = $b_id AND `m_name` = $model" , true );
    //$models = get_all_model($b_id);
    print_r(json_encode($country_name));
}




add_action( 'wp_ajax_get_model_by_brand', 'get_model_by_brand' );
add_action( 'wp_ajax_nopriv_get_model_by_brand', 'get_model_by_brand' );
function get_model_by_brand() {
    global $wpdb; // this is how you get access to the database
    $b_id = intval( $_POST['brand_id'] );
    $table_car_info  = $wpdb->prefix .'car_info';
    $models = $wpdb->get_results("SELECT DISTINCT `m_name` FROM $table_car_info WHERE `b_id` = $b_id " , true );
    //$models = get_all_model($b_id);
    print_r(json_encode($models));
}


add_action( 'wp_ajax_get_generation_by_brand_and_model', 'get_generation_by_brand_and_model' );
add_action( 'wp_ajax_nopriv_get_generation_by_brand_and_model', 'get_generation_by_brand_and_model' );
function get_generation_by_brand_and_model()
{
    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $generations = $wpdb->get_results("SELECT  * FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id " , true );
    $generation_new = array();
    foreach ($generations as $generation){
        $generation_new[get_generation_name_by_id($generation->g_id)] = $generation->g_id;
    }
    print_r(json_encode($generation_new));
}


add_action( 'wp_ajax_get_engine_by_brand_model_and_generation', 'get_engine_by_brand_model_and_generation' );
add_action( 'wp_ajax_nopriv_get_engine_by_brand_model_and_generation', 'get_engine_by_brand_model_and_generation' );
function get_engine_by_brand_model_and_generation() {
    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $g_id =  $_POST['generation_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $engines = $wpdb->get_results("SELECT DISTINCT `e_id` FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id  AND `g_id` = $g_id" , true );
    $engine_new = array();
    foreach ($engines as $engine){
        $engine_new[get_engine_name_by_id($engine->e_id)] = $engine->e_id;
    }
    print_r(json_encode($engine_new));
}

add_action( 'wp_ajax_get_engine_details_by_brand_model_and_generation_and_engine', 'get_engine_details_by_brand_model_and_generation_and_engine' );
add_action( 'wp_ajax_nopriv_get_engine_details_by_brand_model_and_generation_and_engine', 'get_engine_details_by_brand_model_and_generation_and_engine' );
function get_engine_details_by_brand_model_and_generation_and_engine() {

    global $wpdb; // this is how you get access to the database
    $b_id =  $_POST['brand_id'] ;
    $m_id =  $_POST['model_id'];
    $g_id =  $_POST['generation_id'];
    $e_id =  $_POST['engine_id'];
    $table_engine  = $wpdb->prefix .'engine';
    $engines = $wpdb->get_results("SELECT  * FROM $table_engine WHERE `b_id` = $b_id AND `m_id` = $m_id  AND `g_id` = $g_id AND `e_id` = $e_id" , true );
    print_r(json_encode($engines));

   }


function show_search_form($atts){
    $a = shortcode_atts( array(
        'action' => '',
    ), $atts );
    include_once("client_include/form.php");
}
add_shortcode('autoconfigrator_form', 'show_search_form');




function show_search_sidebar_form($atts){
    $a = shortcode_atts( array(
        'action' => '',
    ), $atts );
    include_once("client_include/sidebar_form.php");
}
add_shortcode('autoconfigrator_sidebar_form', 'show_search_sidebar_form');






function show_search_result(){
    include_once("client_include/search_result.php");
}
add_shortcode('autoconfigrator_result', 'show_search_result');




function show_autoconfigrator(){
    include_once("client_include/show_autoconfigrator.php");
}
add_shortcode('autoconfigrator', 'show_autoconfigrator');

add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {
    wp_enqueue_script( 'test', plugins_url( '/test.js', __FILE__ ), array('jquery'), '1.0', true );
}


