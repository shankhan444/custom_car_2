<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 8/7/2016
 * Time: 6:43 PM
 */

$all_brands = get_all_brand();
?>
    <form action="<?php echo $a['action'];?>" name="custom_car_form"  class="custom_car_form" id="custom_car_form" method="post">
        <select class="brand_id" name="brand_id" required>
            <option value=""> Select Brand</option>
            <?php foreach ( $all_brands as $brand ) {?>
                <option value="<?php echo $brand->b_id ;?>"> <?php echo $brand->b_name ;?> </option>
            <?php } ?>
        </select>
        <select class="search model_name" name="model_name" required>
            <option value=""> Select Model</option>
        </select>
        <select class="search country_name " name="country_name" required>
            <option value="">Select Country</option>
        </select>
        <input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">
        <button type="submit" name="car_search" class="search result_button" id="get_result_btn">GO</button>
    </form>