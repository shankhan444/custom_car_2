<?php
global $wpdb;
$table_brand = $wpdb->prefix .'brand';
$all_brands = $wpdb->get_results("SELECT * FROM $table_brand");
?>
<h2>Car Details</h2>
<table width="100%">
    <?php foreach($all_brands as $brand){?>
    <tr>
        <td><?php echo $brand->b_name?></td>
        <td><img src="<?php echo $brand->b_logo?>" height="300px" width="300px"/></td>
    </tr>
    <?php } ?>
</table>
<div class="clear-fix"></div>