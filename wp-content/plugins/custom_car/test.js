jQuery(document).ready(function ($) {
    $('.brand_id').change(function () {
        $('.model_name').empty();
        $('.model_name').append('<option value=""> Loading...</option>');
        var data = {
            'action': 'get_model_by_brand',
            'brand_id': $('.brand_id').val()     // We pass php values differently!
        };
        $.ajax({
            url : $('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                $('.model_name').empty();
                //console.log(response);
                response = response.slice(0,-1);
                response = $.parseJSON(response);
                $('.model_name').append('<option value=""> Select Model</option>');
                $.each(response , function (model_index , model_name) {
                    console.log(model_name);
                   $('.model_name').append('<option value="'+model_name.m_name+'">' + model_name.m_name +'</option>');
                });
            }
        });
    });


    $('.model_name').change(function () {
        $('.country_name').empty();
        $('.country_name').append('<option value=""> Loading...</option>');
        var data = {
            'action': 'get_country_by_brand_and_model',
            'brand_id': $('.brand_id').val(),
            'model_name': $('.model_name').val()     // We pass php values differently!
        };
        $.ajax({
            url : $('.ajax_path').val(),
            method : 'post',
            data : data ,
            success : function (response) {
                $('.country_name').empty();
                //console.log(response);
                response = response.slice(0,-1);
                response = $.parseJSON(response);
                $('.country_name').append('<option value=""> Select Country</option>');
                $.each(response , function (country_index , country_name) {
                    $('.country_name').append('<option value="'+country_name.c_country+'">' +country_name.c_country+'</option>');
                });
            }
        });
    });


     $('#get_result_btn').click(function (e) {
        e.preventDefault();

         if($('.brand_id').val() == ""){
             alert('Please Select Brand');
             return;
         }
         if($('.model_name').val() == "" ){
             alert('Please Select Model');
             return;
         }
         if( $('.country_name').val() == ""){
             alert('Please Select Country');
             return;
         }

         var data = {
             'action': 'get_result_for_car',
             'brand_id': $('.brand_id').val(),
             'model_name': $('.model_name').val(),    // We pass php values differently!
             'country_name': $('.country_name').val()     // We pass php values differently!
         };
         $.ajax({
             url : $('.ajax_path').val(),
             method : 'post',
             data : data ,
             success : function (response) {
                 response = response.slice(0,-1);
                 //response = $.parseJSON(response);
                 $('.div_for_ajax_result').html(response);
             }
         });

     });
});