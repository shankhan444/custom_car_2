<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>
<?php
global $wpdb;
$page_url = get_admin_url()."admin.php?page=custom_car_review";
$table_review = $wpdb->prefix .'car_review';
$table_engine = $wpdb->prefix .'engine';

if(isset($_POST['e_id'])){
		$engine_id = $_POST['e_id'];
}else{
	 	$engine_id = $_GET['custom_car_engine_id'];
}
$redirect_page  ="$page_url&custom_car_engine_id=$engine_id";
if(isset($_POST['insert_review'])):
	$data_array = array(
		'e_id' => $engine_id,
		'r_title' => $_POST['r_title'],
		'r_name' => $_POST['r_name'],
		'r_rating' => $_POST['r_rating'],
		'r_car' => $_POST['r_car'],
		'r_message' => $_POST['r_message'],
	);
	$wpdb->insert($table_review ,$data_array);
	echo "<script>
			window.location.assign($redirect_page);
		</script>";
endif;

if(isset($_GET['custom_car_review_delete'])):
	$data_array = array(
		'r_id' => $_GET['r_id'],
	);
	$wpdb->delete($table_review , $data_array);
	echo "<script>
			window.location.assign('$redirect_page');
		</script>";
endif;

if(isset($_GET['custom_car_review_edit']) && isset($_GET['r_id'])){
	$_review_id = $_GET['r_id'];

	$all_review = $wpdb->get_results("SELECT * FROM $table_review WHERE r_id = $_review_id " , true );



}

if(isset($_POST['update_review'])):
	$data_array = array(
		'e_id' => $engine_id,
		'r_title' => $_POST['r_title'],
		'r_name' => $_POST['r_name'],
		'r_rating' => $_POST['r_rating'],
		'r_car' => $_POST['r_car'],
		'r_message' => $_POST['r_message'],
	);
	$where = array(
		'r_id' => $_POST['r_id']
	);
	$wpdb->update( $table_review , $data_array, $where);
	echo "<script>
			window.location.assign('$redirect_page');
		</script>";
endif;

$all_reviews = $wpdb->get_results("SELECT * FROM $table_review WHERE e_id = $engine_id");

$engine_details = $wpdb->get_results("SELECT * FROM $table_engine WHERE e_id = $engine_id");

?>
<?php if(isset($_GET['custom_car_review_edit']) && isset($_GET['r_id'])){ ?>
	<form id="custom-car-review" method="post" >
		<input type="hidden" value="<?php echo $engine_id ?>" name="e_id">
		<input type="hidden" name="r_id" value="<?php echo $all_review[0]->r_id ?>">
		<table>
			<tr>
			<th> Engine Name </th>
				<td><?php echo get_engine_name_by_id($engine_id); ?></td>
			</tr>
			<tr>
				<td><label>Review Title</label></td>
				<td>
					<input type="text" name="r_title" value="<?php echo  $all_review[0]->r_title?>" required>
				</td>
			</tr>
			<tr>
				<td><label>Review Name </label></td>
				<td>
					<input type="text" name="r_name" value="<?php echo  $all_review[0]->r_name ?>"  required>
				</td>
			</tr>
			<tr>
				<td><label>Review Rating</label></td>
				<td>
					<input type="text" name="r_rating" value="<?php echo  $all_review[0]->r_rating ?>"  required>
				</td>
			</tr>
			<tr>
				<td><label>Review Car</label></td>
				<td>
					<input type="text" name="r_car" value="<?php echo  $all_review[0]->r_car ?>"  required>
				</td>
			</tr>
			<tr>
				<td><label>Review Message</label></td>
				<td><input type="text" name="r_message" value="<?php echo  $all_review[0]->r_message ?>"  required></td>
			</tr>

		</table>
		<button type="submit" name="update_review">Update review</button>
	</form>
<?php }

else { ?>
	<form id="custom-car-review" method="post" >
		<input type="hidden" value="<?php echo $engine_id ?>" name="e_id">
		<table>
			<tr >
				<th> Engine Name </th>
				<td><?php echo get_engine_name_by_id($engine_id); ?></td>
			</tr>
			<tr>
				<td><label>Review Title</label></td>
				<td>
					<input type="text" name="r_title" required>
				</td>
			</tr>
			<tr>
				<td><label>Review Name </label></td>
				<td>
					<input type="text" name="r_name" required>
				</td>
			</tr>
			<tr>
				<td><label>Review Rating</label></td>
				<td>
					<input type="text" name="r_rating" required>
				</td>
			</tr>
			<tr>
				<td><label>Review Car</label></td>
				<td>
					<input type="text" name="r_car" required>
				</td>
			</tr>
			<tr>
				<td><label>Review Message</label></td>
				<td>
					<input type="text" name="r_message" required>
				</td>
			</tr>
		</table>
		<button type="submit" name="insert_review">Add review</button>
	</form>
<?php } ?>
<div class="custom_car_container">
<table width="100%">
	<tr>
		<th>Review Id</th><th>Review Title</th> <th> Review  Name </th> <th> Review Rating </th> <th>Review Car</th> <th>ACTION</th>
	</tr>
	<?php foreach($all_reviews as $review_row){?>
		<tr>
			<td><?php print_r($review_row->r_id) ?></td>
			<td><?php echo $review_row->r_title?></td>
			<td> <?php echo $review_row->r_name ?></td>
			<td> <?php echo $review_row->r_rating ?></td>
			<td><?php echo $review_row->r_car ?></td>
			<td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_review&custom_car_review_edit=yes&r_id='.$review_row->r_id.'&custom_car_engine_id='.$engine_id ?>">Edit</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_review&custom_car_review_delete=yes&r_id='.$review_row->r_id.'&custom_car_engine_id='.$engine_id ?>">Delete</a> </td>
		</tr>
	<?php } ?>
	</table>
	</div>