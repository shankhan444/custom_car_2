<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>
<?php
global $wpdb;
$page_url = get_admin_url()."admin.php?page=custom_car_generation";

$all_brands = get_all_brand();

$table_generation = $wpdb->prefix .'generation';
if(isset($_POST['insert_generation'])):
	$data_array = array(
		'b_id' => $_POST['brand_id'],
		'm_id' => $_POST['model_id'],
		'g_name' => $_POST['generation_name'],

	);
	$wpdb->insert($table_generation ,$data_array);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_generation_delete'])):
	$data_array = array(
		'g_id' => $_GET['custom_car_generation_id'],
	);
	$wpdb->delete($table_generation , $data_array);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_generation_edit']) && isset($_GET['custom_car_generation_id'])){
	$_generation_id = $_GET['custom_car_generation_id'];
	$data_array = array(
		'g_id' => $_generation_id,
	);
	$all_generation = $wpdb->get_results("SELECT * FROM $table_generation WHERE g_id = $_generation_id " , true );

	$models_by_brands = get_all_model($all_generation[0]->b_id);
}

if(isset($_POST['update_generation'])):
	$data_array = array(
		'b_id' => $_POST['brand_id'],
		'm_id' => $_POST['model_id'],
		'g_name' => $_POST['generation_name']
	);
	$where = array(
		'g_id' => $_POST['generation_id']
	);
	$wpdb->update( $table_generation , $data_array, $where);
	echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

$all_generations = $wpdb->get_results("SELECT * FROM $table_generation");
?>
<div class="custom_car_container">
<?php if(isset($_GET['custom_car_generation_edit']) && isset($_GET['custom_car_generation_id'])){ ?>
	<form id="custom-car-generation" method="post">
		<select name="brand_id" class="brand_id" id="brand_id" required>
			<option value=""> Select Brand </option>
			<?php foreach ($all_brands as $brand){
				if($all_generation[0]->b_id == $brand->b_id)
					$selected = "SELECTED";
				else
					$selected = "";
				?>

				<option value="<?php echo $brand->b_id ?>" <?php echo $selected ?> ><?php echo $brand->b_name ?></option>
			<?php } ?>
		</select>

		<select name="model_id" id="model_id" class="model_id" required>
			<?php foreach ($models_by_brands as $models){
				if($all_generation[0]->m_id == $models->m_id)
					$selected = "SELECTED";
				else
					$selected = "";
				?>
				<option value="<?php echo $models->m_id ?>" <?php echo $selected ?> ><?php echo $models->m_name ?></option>
			<?php } ?>

		</select>
		<input type="text" name="generation_name" id="generation_name" value="<?php echo $all_generation[0]->g_name ?>">

		<input type="hidden" name="generation_id" value="<?php echo $all_generation[0]->g_id ?>">
		<button type="submit" name="update_generation">Update generation</button>
	</form>
<?php }else { ?>

	<form id="custom-car-generation" method="post">
		<select name="brand_id" class="brand_id" id="brand_id" required>
			<option value=""> Select Brand </option>
			<?php foreach ($all_brands as $brand){?>
			<option value="<?php echo $brand->b_id ?>"><?php echo $brand->b_name ?></option>
			<?php } ?>
		</select>

		<select name="model_id" id="model_id" class="model_id" required></select>
		<input required type="text" name="generation_name" placeholder="Generation Name" id="generation_name" class="hidden">
		<button type="submit" name="insert_generation">Add Generation</button>
	</form>

<?php } ?>
	<input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">
<table width="100%">
	<tr>
		<th> generation Id</th><th> Brand Name</th><th> Model Name</th> <th> generation Name</th><th> Actions </th>
	</tr>
	<?php foreach($all_generations as $generation_row){?>
		<tr>
			<td><?php print_r($generation_row->g_id) ?></td><td><?php echo get_brand_name_by_id($generation_row->b_id) ?></td><td><?php echo get_model_name_by_id($generation_row->m_id) ?></td><td><?php print_r($generation_row->g_name) ?></td>  <td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_generation&custom_car_generation_edit=yes&custom_car_generation_id='.$generation_row->g_id ?>">Edit</a> | <a href="<?php echo get_admin_url().'admin.php?page=custom_car_generation&custom_car_generation_delete=yes&custom_car_generation_id='.$generation_row->g_id ?>">Delete</a> </td>
		</tr>
	<?php } ?>
</table>
</div>
<script>
	jQuery(document).ready(function ($) {
		$('#brand_id').change(function () {
			$('#model_id').empty();
			$('#generation_name').val();
			$('#generation_name').addClass('hidden');
			if($('#brand_id').val() == "")
				return ;

			var data = {
				'action': 'get_model_by_brand',
				'brand_id': $('#brand_id').val()     // We pass php values differently!
			};
			$.ajax({
				url : $('.ajax_path').val(),
				method : 'post',
				data : data ,
				success : function (response) {
					console.log(response);

					response = response.slice(0,-1);
					console.log(response);
					response = JSON.parse(response);

					//console.log(response);
					$('#model_id').append('<option value=""> Select Model</option>');
					$.each(response , function (model_name , model_index) {
						$('#model_id').append('<option value="'+model_index+'">' + model_name +'</option>')
					});

				}
			});
		});
		$('#model_id').change(function (){
			$('#generation_name').removeClass('hidden');
		});

	});
</script>