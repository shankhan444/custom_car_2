<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 9/5/2016
 * Time: 10:49 PM
 */
if (!function_exists('wp_handle_upload')) {
    require_once(ABSPATH . 'wp-admin/includes/file.php');
}

$uploadedfile = $_FILES['upload_sheet'];

$upload_overrides = array('test_form' => false);

$movefile = wp_handle_upload($uploadedfile, $upload_overrides);

if ($movefile && !isset($movefile['error'])) {
    $file_name = $movefile['file'];
    $file_ext = strtolower(substr($file_name, strrpos($file_name, '.') + 1));
    if ($file_ext == "xlsx") {
        $xlsx = new SimpleXLSX($file_name);
        $i = 0;
        $array_of_all_file = $xlsx->rows();
        $all_car_details = array();
        //$array_of_all_file = array_unique($array_of_all_file , SORT_REGULAR);
        $all_brand = array();
        $j = 0;
        foreach ($array_of_all_file as $data) {
            if($j != 0){
                array_push( $all_brand ,$data[0]);
                array_push($all_car_details ,array($data[0] ,$data[1],$data[2],$data[3]));
            }
            ++$j;
        }
        $all_brand = array_unique($all_brand);
        insert_brand_dump($all_brand);

        $all_brand = get_all_brand_names_array();
        $i = 0;
        $new_all_car_data = array();
        foreach ($all_car_details as $key => $value){
            $brand  = array_search($value[0], $all_brand);
            array_push( $new_all_car_data , array($brand ,$value[1], $value[2] ,$value[3]));
            ++$i;
        }
        //echo $i;
        insert_car_dump($new_all_car_data);
        //echo "</table>";
        echo "<h2> Database Imported Successfully  </h2>";

    } else {
        echo "This Is not valid XLSX file";
        exit;
    }
} else {
    echo $movefile['error'];
    exit;
}