<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
?>
<?php
	global $wpdb;
	$page_url = get_admin_url()."admin.php?page=custom_car_brand";
	$table_brand = $wpdb->prefix .'brand';
	if(isset($_POST['insert_brand'])):

		if(! function_exists('wp_handle_upload')){
		require_once (ABSPATH . "wp-admin/includes/file.php");
		}

		$uploadedfile = $_FILES['brand_logo'];
		$upload_overrides = array('test_form'=>false);

		$moveupload = wp_handle_upload($uploadedfile , $upload_overrides);
		if( $moveupload && ! isset($moveupload['error'])){
			var_dump($moveupload);
		}
		else{
			echo $moveupload['error'];
		}

		$data_array = array(
			'b_name' => $_POST['brand_name'],
			'b_logo' => $moveupload['url']
		);
		$wpdb->insert($table_brand ,$data_array);
		echo "<script>
			window.location.assign('$page_url');
		</script>";
	endif;
	if(isset($_GET['custom_car_brand_delete'])):
		$data_array = array(
			'b_id' => $_GET['custom_car_brand_id'],
		);
		$wpdb->delete($table_brand , $data_array);
		echo "<script>
			window.location.assign('$page_url');
		</script>";
	endif;
	 if(isset($_GET['custom_car_brand_edit']) && isset($_GET['custom_car_brand_id'])){
	    $_brand_id = $_GET['custom_car_brand_id'];
		 $data_array = array(
			 'b_id' => $_brand_id,
		 );
		$all_brand = $wpdb->get_results("SELECT * FROM $table_brand WHERE b_id = $_brand_id " , true );
	 }
	 if(isset($_POST['update_brand'])):

		 if(isset($_FILES['brand_logo']) && !empty($_FILES['brand_logo']['name']))
		 {

			 if(! function_exists('wp_handle_upload')){
				 require_once (ABSPATH . "wp-admin/includes/file.php");
			 }

			 $uploadedfile = $_FILES['brand_logo'];
			 $upload_overrides = array('test_form'=>false);

			 $moveupload = wp_handle_upload($uploadedfile , $upload_overrides);
			 if( $moveupload && ! isset($moveupload['error'])){
				 var_dump($moveupload);
			 }
			 else{
				 echo $moveupload['error'];
			 }

			 $data_array = array(
				 'b_name' => $_POST['brand_name'],
				 'b_logo' => $moveupload['url']
			 );
		 }else{
			 $data_array = array(
				 'b_name' => $_POST['brand_name']
			 );
		 }




		 $where = array(
			'b_id' => $_POST['brand_id']
		 );
		 $wpdb->update( $table_brand , $data_array, $where);
		 echo "<script>
			window.location.assign('$page_url');
		</script>";
     endif;
	$all_brands = $wpdb->get_results("SELECT * FROM $table_brand");
?>


<div class="custom_car_container">
	<?php if(isset($_GET['custom_car_brand_edit']) && isset($_GET['custom_car_brand_id'])){ ?>
    <h1 style="color: #00A8EF;
    background: #cafaca;
    padding: 12px;
    border-radius: 16px;"> Update Brand </h1>
        <form id="custom-car-brand" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <th><label>Brand Name</label></th> <th><label>Add New Icon</label></th> <th><label>Brand Icon</label></th><th><label>Action</label> </th>
        </tr>
        <tr>
            <td>
                <input type="text" name="brand_name" value="<?php echo $all_brand[0]->b_name ?>">
                <input type="hidden" name="brand_id" value="<?php echo $all_brand[0]->b_id ?>">
            </td>
            <td>
                <input type="file" name="brand_logo" onchange="readURL(this);">
            </td>
            <td> <img id="blah"  src="<?php echo $all_brand[0]->b_logo ?>"  height="72px" width="72px" style="    margin-bottom: -13px;"></td>
            <td>
                <button type="submit" name="update_brand">Update Brand</button>
            </td>
        </tr>




		</table>
        </form>
	<?php } else { ?>
        <h1     style="background: #cafaca;
                padding: 12px;
                border-radius: 16px;"> Add New Brand </h1>

        <form id="custom-car-brand" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <th><label>Brand Name</label></th> <th><label>Brand Icon</label></th> <th><label>Action</label> </th>
            </tr>
            <tr>

                   <td>
                       <input type="text" name="brand_name" placeholder="Brand Name" required>
                   </td>

                    <td>
                        <input type="file" name="brand_logo"  onchange="readURL(this);" required>
                        <img src="#" id="blah" style="display: none;">
                    </td>

                <td>
                    <button type="submit" name="insert_brand">Add Brand</button>
                </td>


            </tr>
        </table>
        </form>
	<?php } ?>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result)
                        .width(72)
                        .height(72);
                }

                reader.readAsDataURL(input.files[0]);
                $('#blah').css('display','inline')
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    </script>


    <table width="100%">
		<tr>
			<th> Brand Id</th><th> Brand Name</th><th> Brand LOGO</th><th> Actions </th>
		</tr>
		<?php foreach($all_brands as $brand_row){?>
			<tr>
				<td><?php print_r($brand_row->b_id) ?></td><td><?php print_r($brand_row->b_name) ?></td> <td><img src="<?php print_r($brand_row->b_logo) ?>" height="72px" width="72px"/> </td> <td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_brand&custom_car_brand_edit=yes&custom_car_brand_id='.$brand_row->b_id ?>">Edit</a> | <a  class="delete_brand_id" data-id="<?php echo $brand_row->b_id ?>" href="<?php echo get_admin_url().'admin.php?page=custom_car_brand&custom_car_brand_delete=yes&custom_car_brand_id='.$brand_row->b_id ?>">Delete</a> </td>
			</tr>
		<?php } ?>
	</table>
</div>
<input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">

