/**
 * Created by SHAN on 8/11/2016.
 */

jQuery(document).ready(function ($) {
    $('.delete_brand_id').click(function (e) {
       e.preventDefault();
        var brand_id = $(this).data('id');

        alertify.confirm("Delete Brand ","Are You Sure To Delete This Brand.",
            function(){
                //alertify.success('Ok');
                //alert(brand_id);



                var data = {
                    'action': 'delete_brand_ajax',
                    'brand_id': brand_id
                };
                $.ajax({
                    url : $('.ajax_path').val(),
                    method : 'post',
                    data : data ,
                    success : function (response) {
                        location.reload();
                    }
                });

            },
            function(){
                // alertify.error('Cancel');
               // alert('Fail');
            });

    });

    $('.delete_car_id').click(function (e) {
        e.preventDefault();
        var car_id = $(this).data('id');

        alertify.confirm("Delete Car ","Are You Sure To Delete This Car Info.",
            function(){
                //alertify.success('Ok');
                //alert(car_id);
                var data = {
                    'action': 'delete_car_ajax',
                    'car_id': car_id
                };
                $.ajax({
                    url : $('.ajax_path').val(),
                    method : 'post',
                    data : data ,
                    success : function (response) {
                        location.reload();
                    }
                });

            },
            function(){
                // alertify.error('Cancel');
                // alert('Fail');
            });

    });

});
