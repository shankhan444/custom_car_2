<?php
/**
 * Created by PhpStorm.
 * User: SHAN
 * Date: 7/19/2016
 * Time: 11:22 PM
 */
if(isset($_POST['upload_xslx'])){
    include_once ('upload_import.php');
}

?>

<style>

    #loader-wrapper{
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.58);;
        z-index: 10000;
    }

    #loader {
        z-index: 1001; /* anything higher than z-index: 1000 of .loader-section */
    }

    h1 {
        color: #EEEEEE;
    }

    #loader-wrapper{
        color: white;
    }

    #loader.loader-section img{
        display: block;
        margin-left: auto;
        margin-right: auto;
        margin: 0 auto;
    }

    .loader-section img{

        margin-top: 250px;
        /*
        margin-left: 40%;
        */
        height: 200px;
        width: 200px;
    }

</style>
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" type="text/css">

<style>
    a.dt-button {
        padding: 6px;
        border: 2px solid black;
        display: inline-block;
        margin: 2px 4px;
        background-color: #FFF;
        box-shadow: 4px 4px 4px #CCC;
    }

    div#example_filter {
        display: inline-block;
        float: right;
        margin: 0px 80px;
    }

    .dt-buttons {
        display: inline-block;
    }

    tr.odd {
        background-color: white;
    }

    a#example_previous , a#example_next {
        padding: 6px;
        border: 2px solid black;
        display: inline-block;
        margin: 2px 4px;
        background-color: #FFF;
        box-shadow: 4px 4px 4px #CCC;
        border-radius: 6px;
    }

    a.paginate_button {
        padding: 10px;
    }

    td {
        font-family: inherit;
        font-size: inherit;
        font-weight: inherit;
        line-height: inherit;
        padding: 6px 0px;
        /*border-bottom: 1px solid #7e7c7c; */
    }

    th {
        text-align: left;
        background-color: black;
        /* padding: 10px; */
        /* box-shadow: 20px 20px 20px #CCC; */
        color: white;
        padding: 10px;
        border: 1px dashed #CCC;
    }

    .nowrap tr:nth-child(even){
        background-color:white;
    }

</style>



<div id="loader-wrapper">
    <div id="loader">
        <div class="loader-section">
            <center>
                <img src="<?php echo plugins_url() ?>/custom_car/load.gif">
            </center>

        </div>
    </div>


</div>

<script>
    jQuery('#loader-wrapper').show();
</script>
<?php
global $wpdb;
$page_url = get_admin_url()."admin.php?page=custom_car_main_menu";
$table_car_info = $wpdb->prefix .'car_info';
$table_brand = $wpdb->prefix .'brand';
$all_brand = get_all_brand();
$all_engine = get_all_engine();

if(isset($_POST['insert_engine'])):
    $data_array = array(
        'b_id' => $_POST['brand_id'],
        'm_name' => $_POST['model'],
        'c_country' => $_POST['country'],
        'c_url' => $_POST['url'],
    );

    $wpdb->insert($table_car_info ,$data_array);
    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_engine_delete'])):
    $data_array = array(
        'c_id' => $_GET['custom_car_engine_id'],
    );
    $wpdb->delete($table_car_info , $data_array);
    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

if(isset($_GET['custom_car_engine_edit']) && isset($_GET['custom_car_engine_id'])){
    $_engine_id = $_GET['custom_car_engine_id'];
    $all_engine = $wpdb->get_results("SELECT * FROM $table_car_info WHERE c_id = $_engine_id " , true );
    $all_model = get_all_model($all_engine[0]->b_id);
    $all_generation = get_all_generation($all_engine[0]->b_id , $all_engine[0]->m_id);
}

if(isset($_POST['update_engine'])):
    $data_array = array(
        'b_id' => $_POST['brand_id'],
        'm_name' => $_POST['model'],
        'c_country' => $_POST['country'],
        'c_url' => $_POST['url'],
    );
    $where = array(
        'c_id' => $_POST['car_id']
    );
    $wpdb->update( $table_car_info , $data_array, $where);


    $data_array_brand = array(
        'b_name' => $_POST['brand_name']
    );

    $where_brand = array(
        'b_id' => $_POST['brand_id']
    );
    $wpdb->update( $table_brand , $data_array_brand, $where_brand);

    echo "<script>
			window.location.assign('$page_url');
		</script>";
endif;

$all_engines = $wpdb->get_results("SELECT * FROM $table_car_info");
?>
<style>
    .custom_car_container tr:nth-child(odd) {
        background: none;
    }

    .custom_car_container select ,   .custom_car_container input{
        padding: 2px;
        line-height: 28px;
        height: 28px;
        width: 100%;
    }
</style>
<div class="custom_car_container">

    <table>
        <form class="upload_dump_form" id="upload_dump_form" enctype="multipart/form-data" method="post">
            <tr>
                <td><input type="file" name="upload_sheet" id="upload_sheet" class="upload_sheet" required></td>
                <td><button type="submit" name="upload_xslx">Import Database From Excel File </button></td>
            </tr>
        </form>
    </table>

<?php if(isset($_GET['custom_car_engine_edit']) && isset($_GET['custom_car_engine_id'])){ ?>
    <h1 style="color: #00A8EF;background: #cafaca;
                padding: 12px;
                border-radius: 16px;"> Update Car </h1>
    <form id="custom-car-engine" method="post">
        <input type="hidden" name="car_id" value="<?php echo $all_engine[0]->c_id ?>">
        <table>
            <tr>
                <th><label>Brand</label></th> <th><label>Model</label></th> <th><label>Country</label></th> <th><label>URL</label></th>

            </tr>
            <tr>
               <td>
                   <!--
                    <select name="brand_id" id="brand_id" required>
                        <option value="<?php echo $all_engine[0]->b_id?>"> <?php echo get_brand_name_by_id($all_engine[0]->b_id) ?></option>
                        <?php foreach ($all_brand as $brand){?>
                            <option value="<?php echo $brand->b_id?>"> <?php echo $brand->b_name ?></option>
                        <?php } ?>
                    </select>
                    -->
                   <input type="text" name="brand_name" value="<?php echo get_brand_name_by_id($all_engine[0]->b_id) ?>">
                   <input type="hidden" name="brand_id" value="<?php echo $all_engine[0]->b_id ?>">
                </td>
                <td>
                    <input type="text" name="model" placeholder="Add Model" value="<?php echo $all_engine[0]->m_name?>"> </td>
                <td><input type="text" name="country" placeholder="Add Country" value="<?php echo $all_engine[0]->c_country?>"> </td>
                <td><input type="url" name="url" placeholder="Add URL"  value="<?php echo $all_engine[0]->c_url?>"> </td>
            </tr>

        </table>
        <button type="submit" name="update_engine">Update Car</button>
    </form>
<?php }

else { ?>
    <h1 style="color: #000;background: #cafaca;
                padding: 12px;
                border-radius: 16px;">  Add New Car </h1>
    <form id="custom-car-engine" method="post">

        <table>
            <tr>
                <th><label>Brand</label></th> <th><label>Model</label></th> <th><label>Country</label></th> <th><label>URL</label></th>

            </tr>
            <tr>
                <td>
                    <select name="brand_id" id="brand_id" required>
                        <option value=""> Select Brand</option>
                        <?php foreach ($all_brand as $brand){?>
                            <option value="<?php echo $brand->b_id?>"> <?php echo $brand->b_name ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td><input type="text" name="model" placeholder="Add Model"> </td>
                <td><input type="text" name="country" placeholder="Add Country"> </td>
                <td><input type="url" name="url" placeholder="Add URL"> </td>
            </tr>
            <tr class="engine_details_row">
                <td colspan="2"> <button type="submit" name="insert_engine">Add Car</button></td>
            </tr>
        </table>
    </form>
<?php } ?>
    <input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">
    <table id="example" class="display nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th> Brand </th><th>Model</th> <th> Country </th> <th> URL </th>  <th>Action</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th> Brand </th><th>Model</th> <th> Country </th> <th> URL </th>  <th>Action</th>
        </tr>
        </tfoot>
        <tbody>
    <?php foreach($all_engines as $engine_row){?>
        <tr>
            <td><?php echo get_brand_name_by_id($engine_row->b_id) ?></td>
            <td><?php echo $engine_row->m_name?></td>
            <td> <?php echo $engine_row->c_country ?></td>
            <td> <a href="<?php echo $engine_row->c_url ?>" target="_blank"><?php echo $engine_row->c_url ?></a></td>
            <td> <a href="<?php echo get_admin_url().'admin.php?page=custom_car_main_menu&custom_car_engine_edit=yes&custom_car_engine_id='.$engine_row->c_id ?>">Edit</a> | <a  class="delete_car_id"   data-id="<?php echo $engine_row->c_id ?>" href="<?php echo get_admin_url().'admin.php?page=custom_car_main_menu&custom_car_engine_delete=yes&custom_car_engine_id='.$engine_row->c_id ?>">Delete</a> </td>
        </tr>
    <?php } ?>
        </tbody>
</table>
</div>
<input class="ajax_path" value="<?php echo admin_url('admin-ajax.php');  ?>" type="hidden">


<script src="//code.jquery.com/jquery-1.12.3.js"> </script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"> </script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"> </script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"> </script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"> </script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"> </script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"> </script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"> </script>

<script>

    jQuery(document).ready(function ($) {
        $('#loader-wrapper').hide();

        $('#brand_id').change(function () {
            $('#model_id').empty();
            $('#generation_id').empty();
            //$('#generation_name').addClass('hidden');
            if($('#brand_id').val() == "")
                return ;

            var data = {
                'action': 'get_model_by_brand',
                'brand_id': $('#brand_id').val()     // We pass php values differently!
            };
            $.ajax({
                url : $('.ajax_path').val(),
                method : 'post',
                data : data ,
                success : function (response) {
                    console.log(response);

                    response = response.slice(0,-1);
                    console.log(response);
                    response = JSON.parse(response);

                    //console.log(response);
                    $('#model_id').append('<option value=""> Select Model</option>');
                    $.each(response , function (model_name , model_index) {
                        $('#model_id').append('<option value="'+model_index+'">' + model_name +'</option>')
                    });

                }
            });
        });
        $('#model_id').change(function (){
                $('#generation_id').empty();
                if($('#model_id').val() == "")
                    return ;
                var data = {
                    'action': 'get_generation_by_brand_and_model',
                    'brand_id': $('#brand_id').val(),     // We pass php values differently!
                    'model_id': $('#model_id').val()     // We pass php values differently!
                };
                $.ajax({
                    url : $('.ajax_path').val(),
                    method : 'post',
                    data : data ,
                    success : function (response) {
                        console.log(response);

                        response = response.slice(0,-1);
                        console.log(response);

                        response = JSON.parse(response);
                        //console.log(response);
                        $('#generation_id').append('<option value=""> Select Generation </option>');
                        $.each(response , function (generation_name , generation_index) {
                            $('#generation_id').append('<option value="'+generation_index+'">' + generation_name +'</option>')
                        });
                    }
                });


        });

    });
</script>

<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel' , 'print'
            ]
        } );
    } );
</script>
